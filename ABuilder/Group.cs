﻿using System;

namespace ABuilder
{
	public class Group
	{
		//Represents the basic group.
		//Todo:
		//		Add Launch Assets and Weapons
		//		Add default constructor for cloning existing group
		//		Add default constructor for creating group via index

		private faction _faction;
		private string _name;
		private int _points;
		private string _class;
		private string _scan;
		private string _sig;
		private string _thrust;
		private int _hull;
		private string _armour;
		private int _pd;
		private Tuple<int, int> _gsize;
		private int _currentsize;
		private tonnage _tonnage;
		private string _special;

		public Group(faction inFaction, string inName, int inPoints, string inClass, string inScan, string inSig,
			string inThrust, int inHull, string inArmour, int inPD, int inGMin, int inGMax, tonnage inTonnage, string inSpecial)
		{
			_faction = inFaction;
			_name = inName;
			_points = inPoints;
			_class = inClass;
			_scan = inScan;
			_sig = inSig;
			_thrust = inThrust;
			_hull = inHull;
			_armour = inArmour;
			_pd = inPD;
			_gsize = new Tuple<int, int>(inGMin, inGMax);
			_currentsize = inGMin;
			_tonnage = inTonnage;
			_special = inSpecial;
		}

		public Group(shipData inShip)
			: this(inShip.Faction, inShip.Name, inShip.Points, inShip.Class, inShip.Sig, inShip.Sig, inShip.Thrust, inShip.Hull, inShip.Armour, inShip.PD, inShip.GroupSize.Item1, inShip.GroupSize.Item2, inShip.Tonnage, inShip.Special){}

		public faction Faction {
			//Ship's class
			get {
				return _faction;
			}
		}

		public string Name {
			//Ship's name
			get {
				return _name;
			}
		}

		public int Points {
			//How much the ship costs
			get {
				return _points;
			}
		}

		public string Class {
			//Returns the ship's class ie subtitle
			get {
				return _class;
			}
		}

		public Tuple<int, int> GroupSize {
			//Returns the group size, min/max
			get {
				return _gsize;
			}
		}

		public int CurrentSize {
			//Returns the current group size
			get {
				return _currentsize;
			}

			set {
				_currentsize = value;
			}
		}

		public tonnage Tonnage {
			//Returns the ship's tonnage
			get {
				return _tonnage;
			}
		}

		public bool Valid {
			//Returns whether the group is valid.
			//Battlegroup etc. will check if points are within limits, but this only checks that there's not too many ships in the group.
			get {
				if (_currentsize > _gsize.Item2) {
					return false;
				} else {
					return true;
				}
			}
		}
	}
}

