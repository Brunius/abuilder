﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;
using System.IO;

namespace ABuilder
{
	public enum faction {
		UCM,	//United Colonies of Mankind
		SCG,	//Scourge
		PHR,	//Post-Human Republic
		STR		//Shaltari
	}

	public enum tonnage {
		L,	//Light
		M,	//Medium
		H,	//Heavy
		S	//Superheavy
	}

	public struct shipData {
		public faction Faction;
		public string Name;
		public int Points;
		public string Class;
		public string Scan;
		public string Sig;
		public string Thrust;
		public int Hull;
		public string Armour;
		public int PD;
		public Tuple<int,int> GroupSize;
		public tonnage Tonnage;
		public string Special;

		public shipData(string rawData) : this() {
			//A single line of raw data from a file
			string[] elements = Regex.Split(rawData, @"(\t|\n)+");
			switch (elements[0]) {
				case "UCM":
					this.Faction = faction.UCM;
					break;
				case "SCG":
					this.Faction = faction.SCG;
					break;
				case "PHR":
					this.Faction = faction.PHR;
					break;
				case "STR":
					this.Faction = faction.STR;
					break;
			}
			this.Name = elements[2];
			this.Points = int.Parse(elements[4]);
			this.Class = elements[6];
			this.Scan = elements[8];
			this.Sig = elements[10];
			this.Thrust = elements[12];
			this.Hull = int.Parse(elements[14]);
			this.Armour = elements[16];
			this.PD = int.Parse(elements[18]);
			this.GroupSize = new Tuple<int, int>(int.Parse(elements[20]), int.Parse(elements[22]));
			switch (elements[24]) {
				case "L":
					this.Tonnage = tonnage.L;
					break;
				case "M":
					this.Tonnage = tonnage.M;
					break;
				case "H":
					this.Tonnage = tonnage.H;
					break;
				case "S":
					this.Tonnage = tonnage.S;
					break;
			}
			this.Special = elements[26];
		}
	}

	class MainClass
	{
		public static void Main(string[] args)
		{
			List<shipData> shipList = new List<shipData>(); //Like a shitlist but a ship list, get it?

			//Read ship data file
			string rawFile = File.ReadAllText("ships.csv");
			//Split data file into individual lines
			string[] elements = Regex.Split(rawFile, @"(\n)+");

			//Compile our master list of ships
			foreach (string rawData in elements) {
				//It's splitting by \n, so about half our array will be \n characters. Those break the struct
				//An empty string also breaks the struct. ¯\(º_o)/¯
				if (!((rawData == "\n")||(rawData == ""))) {
					shipList.Add(new shipData(rawData));
				}
			}

			//Write test data to the console for review
			foreach (shipData test in shipList) {
				Console.WriteLine("Faction:\t" + test.Faction);
				Console.WriteLine("Name:\t\t" + test.Name);
				Console.WriteLine("Points:\t\t" + test.Points);
				Console.WriteLine("Class:\t\t" + test.Class);
				Console.WriteLine("Scan:\t\t" + test.Scan);
				Console.WriteLine("Sig:\t\t" + test.Sig);
				Console.WriteLine("Thrust:\t\t" + test.Thrust);
				Console.WriteLine("Hull:\t\t" + test.Hull);
				Console.WriteLine("Armour:\t\t" + test.Armour);
				Console.WriteLine("PD:\t\t" + test.PD);
				if (test.GroupSize.Item1 == test.GroupSize.Item2) {
					Console.WriteLine("Group Size:\t" + test.GroupSize.Item1);
				} else {
					Console.WriteLine("Group Size:\t" + test.GroupSize.Item1 + "-" + test.GroupSize.Item2);
				}
				Console.WriteLine("Tonnage:\t" + test.Tonnage);
				Console.WriteLine("Special:\t" + test.Special);
			}

			//Hold the console open
			Console.Write("SR14 : Pathfinder group - 316pts\n\tToulon group - 70pts\n\t\tToulon - 35pts\n\t\tToulon - 35pts\n\tToulon group - 70pts\n\t\tToulon - 35pts\n\t\tToulon - 35pts\n\tNew Cairo group - 176pts\n\t\tNew Cairo - 88pts\n\t\tNew Cairo - 88pts\n");
			string discard = Console.ReadLine();
		}
	}
}
