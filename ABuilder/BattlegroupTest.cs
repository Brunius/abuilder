using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace ABuilder
{
	[TestFixture()]
	public class BattlegroupTest
	{
		public Group SHSetup()
		{
			Group explicitGroup = new Group(faction.UCM, "Beijing", 252, "Battleship", "8\"", "12\"", "6\"", 18, "3+", 10, 1, 1, tonnage.S, "");
			//						Faction, Name, Points, Class, Scan, Sig, Thrust, Hull, A, PD, GMin, GMax, T, Special
			return explicitGroup;
		}

		public Group MSetup()
		{
			Group explicitGroup = new Group(faction.UCM, "New Cairo", 88, "Light Cruiser", "6\"", "6\"", "10\"", 8, "4+", 5, 2, 3, tonnage.M, "");
			//						Faction, Name, Points, Class, Scan, Sig, Thrust, Hull, A, PD, GMin, GMax, T, Special
			return explicitGroup;
		}

		public Group LSetup()
		{
			Group explicitGroup = new Group(faction.UCM, "Toulon", 35, "Frigate", "6\"", "3\"", "10\"", 4, "4+", 3, 2, 4, tonnage.L, "");
			//						Faction, Name, Points, Class, Scan, Sig, Thrust, Hull, A, PD, GMin, GMax, T, Special
			return explicitGroup;
		}

		private Battlegroup setup()
		{
			Battlegroup explicitGroup = new Battlegroup("Pathfinder", new List<Tuple<tonnage, int, int,
				List<Group>>>{new Tuple<tonnage, int, int, List<Group>>(tonnage.L, 1, 3, new List<Group>{}), new Tuple<tonnage,
					int, int, List<Group>>(tonnage.M, 0, 1, new List<Group>{})}, 3);
			return explicitGroup;
		}

		[Test()]
		public void SetupTest()
		{
			//Setup
			Battlegroup testBG = setup();

			//Test Name
			Assert.AreEqual("Pathfinder", testBG.Name);

			//Test original group/tuple method of group listing
			Assert.AreEqual(tonnage.L, testBG.Groups[0].Item1);
			Assert.AreEqual(1, testBG.Groups[0].Item3.Item1);
			Assert.AreEqual(3, testBG.Groups[0].Item3.Item2);
			Assert.AreEqual(tonnage.M, testBG.Groups[1].Item1);
			Assert.AreEqual(0, testBG.Groups[1].Item3.Item1);
			Assert.AreEqual(1, testBG.Groups[1].Item3.Item2);

			//Test new group/struct method of group listing
			Assert.AreEqual(tonnage.L, testBG.GroupsBGData[0].Tonnage);
			Assert.AreEqual(1, testBG.GroupsBGData[0].MinNum);
			Assert.AreEqual(3, testBG.GroupsBGData[0].MaxNum);
			Assert.AreEqual(tonnage.M, testBG.GroupsBGData[1].Tonnage);
			Assert.AreEqual(0, testBG.GroupsBGData[1].MinNum);
			Assert.AreEqual(1, testBG.GroupsBGData[1].MaxNum);

			//Test max number of groups
			Assert.AreEqual(3, testBG.MaxGroups);

			//Test if group is valid
			Assert.AreEqual(false, testBG.Valid);
		}

		[Test()]
		public void AddGroupsTest()
		{
			//Setup
			Battlegroup testBG = setup();
			Group testLG = LSetup();
			Group testMG = MSetup();
			testBG.GroupsBGData[0].ListOfGroups.Add(testLG);

			//Test if group matches
			Assert.AreEqual(testLG, testBG.GroupsBGData[0].ListOfGroups[0]);

			//BG is now valid, as it has a Light group.
			Assert.AreEqual(true, testBG.Valid);

			//Test BG SR
			//Should be 2 -> 2 * 1 (Toulon)
			Assert.AreEqual(2, testBG.StrategyRating);

			//Test BG points
			//Should be 70 -> 2 * 35 (Toulon)
			Assert.AreEqual(70, testBG.Points);

			//Add a Medium size group.
			testBG.GroupsBGData[1].ListOfGroups.Add(testMG);

			//Test if group matches
			Assert.AreEqual(testMG, testBG.GroupsBGData[1].ListOfGroups[0]);

			//Is BG still valid? It should be
			Assert.AreEqual(true, testBG.Valid);

			//Test BG SR
			//Should be 12 -> 2 * 1 (Toulon) + 2 * 5 (New Cairo)
			Assert.AreEqual(12, testBG.StrategyRating);

			//Test BG points
			//Should be 246 -> 2 * 35 (Toulon) + 2 * 88 (New Cairo)
			Assert.AreEqual(246, testBG.Points);

			//Test add via Add() method - should autoselect based on tonnage
			testBG.Add(testLG);

			Assert.AreEqual(2, testBG.GroupsBGData[0].ListOfGroups.Count);

			//Is BG still valid? It should be
			Assert.AreEqual(true, testBG.Valid);

			//Test BG SR
			//Should be 14 -> 4 * 1 (Toulon) + 2 * 5 (New Cairo)
			Assert.AreEqual(14, testBG.StrategyRating);

			//Test BG points
			//Should be 316 -> 4 * 35 (Toulon) + 2 * 88 (New Cairo)
			Assert.AreEqual(316, testBG.Points);

			//
			Assert.AreEqual("SR14 : Pathfinder group - 316pts\n\tToulon group - 70pts\n\t\tToulon - 35pts\n\t\tToulon - 35pts\n\tToulon group - 70pts\n\t\tToulon - 35pts\n\t\tToulon - 35pts\n\tNew Cairo group - 176pts\n\t\tNew Cairo - 88pts\n\t\tNew Cairo - 88pts\n", testBG.BattlegroupDataFormatted);
		}
	}
}