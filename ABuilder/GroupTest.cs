﻿using NUnit.Framework;
using System;

namespace ABuilder
{
	[TestFixture()]
	public class GroupTest
	{
		public Group SHSetup()
		{
			Group explicitGroup = new Group(faction.UCM, "Beijing", 252, "Battleship", "8\"", "12\"", "6\"", 18, "3+", 10, 1, 1, tonnage.S, "");
			//						Faction, Name, Points, Class, Scan, Sig, Thrust, Hull, A, PD, GMin, GMax, T, Special
			return explicitGroup;
		}

		public Group MSetup()
		{
			Group explicitGroup = new Group(faction.UCM, "New Cairo", 88, "Light Cruiser", "6\"", "6\"", "10\"", 8, "4+", 5, 2, 3, tonnage.M, "");
			//						Faction, Name, Points, Class, Scan, Sig, Thrust, Hull, A, PD, GMin, GMax, T, Special
			return explicitGroup;
		}

		public Group LSetup()
		{
			Group explicitGroup = new Group(faction.UCM, "Toulon", 35, "Frigate", "6\"", "3\"", "10\"", 4, "4+", 3, 2, 4, tonnage.L, "");
			//						Faction, Name, Points, Class, Scan, Sig, Thrust, Hull, A, PD, GMin, GMax, T, Special
			return explicitGroup;
		}

		public Group shipSHSetup()
		{
			shipData inShip = new shipData("UCM\tBeijing\t252\tBattleship\t8\"\t12\"\t6\"\t18\t3+\t10\t1\t1\tS\t\r");
			Group explicitGroup = new Group(inShip);
			return explicitGroup;
		}

		[Test()]
		public void SetupTest()
		{
			Group testGroup = SHSetup();

			Assert.AreEqual(faction.UCM, testGroup.Faction);
			Assert.AreEqual("Beijing", testGroup.Name);
			Assert.AreEqual(252, testGroup.Points);
			Assert.AreEqual("Battleship", testGroup.Class);
			Assert.AreEqual(new Tuple<int, int>(1,1), testGroup.GroupSize);
			//Group size stored as tuple - min Item1, max Item2
			Assert.AreEqual(1, testGroup.CurrentSize);
			Assert.AreEqual(tonnage.S, testGroup.Tonnage);

			testGroup = MSetup();

			Assert.AreEqual(faction.UCM, testGroup.Faction);
			Assert.AreEqual("New Cairo", testGroup.Name);
			Assert.AreEqual(88, testGroup.Points);
			Assert.AreEqual("Light Cruiser", testGroup.Class);
			Assert.AreEqual(new Tuple<int, int>(2,3), testGroup.GroupSize);
			//Group size stored as tuple - min Item1, max Item2
			Assert.AreEqual(2, testGroup.CurrentSize);
			Assert.AreEqual(tonnage.M, testGroup.Tonnage);

			testGroup = shipSHSetup();

			Assert.AreEqual(faction.UCM, testGroup.Faction);
			Assert.AreEqual("Beijing", testGroup.Name);
			Assert.AreEqual(252, testGroup.Points);
			Assert.AreEqual("Battleship", testGroup.Class);
			Assert.AreEqual(new Tuple<int, int>(1,1), testGroup.GroupSize);
			//Group size stored as tuple - min Item1, max Item2
			Assert.AreEqual(1, testGroup.CurrentSize);
			Assert.AreEqual(tonnage.S, testGroup.Tonnage);
		}

		[Test()]
		public void ChangeTest()
		{
			Group testGroup = MSetup();

			Assert.AreEqual(new Tuple<int, int>(2, 3), testGroup.GroupSize);
			Assert.AreEqual(2, testGroup.CurrentSize);
			testGroup.CurrentSize = 3;
			Assert.AreEqual(3, testGroup.CurrentSize);
			Assert.AreEqual(true, testGroup.Valid);
			testGroup.CurrentSize = 4;
			Assert.AreEqual(4, testGroup.CurrentSize);
			Assert.AreEqual(false, testGroup.Valid);
		}
	}
}