﻿using System;
using System.Collections.Generic;

namespace ABuilder
{
	public class Battlegroup
	{
		private string _name;
		private int _maxgroups;

		public struct BGData
		{
			public tonnage Tonnage;
			public List<Group> ListOfGroups;
			public int MinNum;
			public int MaxNum;

			public BGData(tonnage inTonnage, List<Group> inList, int minSize, int maxSize) : this() {
				this.Tonnage = inTonnage;
				this.ListOfGroups = inList;
				this.MinNum = minSize;
				this.MaxNum = maxSize;
			}
		}

		private List<BGData> _grpList;

		public Battlegroup(string inName, List<Tuple<tonnage, int, int, List<Group>>> inGroups, int inMaxGroup)
		{
			_grpList = new List<BGData>();
			_name = inName;
			_maxgroups = inMaxGroup;
			foreach (Tuple<tonnage, int, int, List<Group>> _tup in inGroups) {
				_grpList.Add(new BGData(_tup.Item1, _tup.Item4, _tup.Item2, _tup.Item3));
			}
		}

		public string Name {
			get {
				return _name;
			}
		}

		public int MaxGroups {
			get {
				return _maxgroups;
			}
		}

		public int StrategyRating {
			get {
				int _i = 0;
				foreach (BGData _dat in _grpList) {
					foreach (Group _grp in _dat.ListOfGroups) {
						switch (_grp.Tonnage) {
							case tonnage.L:
								_i = _i + 1 * _grp.CurrentSize;
								break;
							case tonnage.M:
								_i = _i + 5 * _grp.CurrentSize;
								break;
							case tonnage.H:
								_i = _i + 10 * _grp.CurrentSize;
								break;
							case tonnage.S:
								_i = _i + 15 * _grp.CurrentSize;
								break;
						}
					}
				}
				return _i;
			}
		}

		public List<Tuple<tonnage, List<Group>, Tuple<int, int>>> Groups {
			get {
				List<Tuple<tonnage, List<Group>, Tuple<int, int>>> grps = new List<Tuple<tonnage, List<Group>, Tuple<int, int>>>(_grpList.Count);
				foreach (BGData _dat in _grpList) {
					grps.Add(new Tuple<tonnage, List<Group>, Tuple<int, int>>(_dat.Tonnage, _dat.ListOfGroups, new Tuple<int, int>(_dat.MinNum, _dat.MaxNum)));
				}
				return grps;
			}
		}

		public List<BGData> GroupsBGData {
			get {
				return _grpList;
			}
		}

		public bool Valid {
			get {
				foreach (BGData _tup in _grpList) {
					foreach (Group _grp in _tup.ListOfGroups) {
						if (!_grp.Valid) {
							return false;
						}
					}
					if ((_tup.ListOfGroups.Count < _tup.MinNum) || (_tup.ListOfGroups.Count > _tup.MaxNum)) {
						return false;
					}
				}
				return true;
			}
		}

		public int Points {
			get {
				int _i = 0;
				foreach (BGData _dat in _grpList) {
					foreach (Group _grp in _dat.ListOfGroups) {
						_i = _i + _grp.Points * _grp.CurrentSize;
					}
				}
				return _i;
			}
		}

		public string BattlegroupDataFormatted {
			get {
				string returnString = "SR" + StrategyRating.ToString() + " : " + _name + " group - " + Points.ToString() + "pts\n";
				foreach (BGData _dat in _grpList) {
					foreach (Group _grp in _dat.ListOfGroups) {
						returnString = returnString + "\t" + _grp.Name + " group - " + (_grp.Points * _grp.CurrentSize).ToString() + "pts\n";
						for (int i = _grp.CurrentSize; i > 0; i--) {
							returnString = returnString + "\t\t" + _grp.Name + " - " + _grp.Points.ToString() + "pts\n";
						}
					}
				}
				return returnString;
			}
		}

		public bool Add (Group newGroup) {
			//Returns true if group was successfully added
			//Returns false if group was NOT added
			foreach (BGData _dat in _grpList) {
				if (_dat.Tonnage == newGroup.Tonnage) {
					_dat.ListOfGroups.Add(newGroup);
					return true;
				}
			}
			return false;
		}
	}
}

